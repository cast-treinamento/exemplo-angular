import { Usuario } from './usuario';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    protected http: HttpClient
  ) { }

  salvar(model:Usuario):Observable<any>{
    return this.http.post<any>(environment.urlBackend + "usuario" , model)
  }

}
