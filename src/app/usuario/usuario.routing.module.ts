import { UsuarioConsultaComponent } from './usuario-consulta/usuario-consulta.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UsuarioCadastroComponent } from 'src/app/usuario/usuario-cadastro/usuario-cadastro.component';

const usuarioRoutes : Routes = [
    {
        path:'',
        component:UsuarioConsultaComponent
    },
    {
        path:'cadastro',
        component:UsuarioCadastroComponent
    },

]

@NgModule({
    imports:[RouterModule.forChild(usuarioRoutes)],
    exports:[RouterModule]
})
export class UsuarioRoutingModule{

}