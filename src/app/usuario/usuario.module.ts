import { UsuarioRoutingModule } from './usuario.routing.module';
import { ComumModule } from './../comum.module';
import { UsuarioCadastroComponent } from './usuario-cadastro/usuario-cadastro.component';
import { NgModule } from '@angular/core';

import { UsuarioConsultaComponent } from 'src/app/usuario/usuario-consulta/usuario-consulta.component';

@NgModule({
  imports: [
    ComumModule,
    UsuarioRoutingModule
  ],
  declarations: [
    UsuarioConsultaComponent, 
    UsuarioCadastroComponent
  ]
})
export class UsuarioModule { }
