import { UsuarioService } from './../../usuario.service';
import {
  Component,
  OnInit
} from '@angular/core'
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms'

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioCadastroComponent implements OnInit {

  public form : FormGroup
  public perfis = [
    {
      id:1,
      nome:'Admin'
    },
    {
      id:2,
      nome:'Oreia'
    }
  ]

  constructor(
    private fb: FormBuilder,
    private usuarioService:UsuarioService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      nome : [null, Validators.required],
      idade : [5, [Validators.required, Validators.min(2)]],
      perfil: this.fb.group({
        nome: [null, Validators.required],
        id:[2, Validators.required] 
      })
    })
  }

  salvar(){
    if(this.validateCampos(this.form)){
      this.usuarioService.salvar(this.form.value).subscribe(
        response => {
          alert("salvo com sucesso")
        },
        error => {
          alert("Não salvou")
        }
      )
    }
  }

  validateCampos(formGroup: FormGroup) {
    let erro: number = 0;
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        if (control.errors) {
          erro++;
        }
      } else if (control instanceof FormGroup) {
        this.validateCampos(control);
      }
    });
    return erro === 0;
  }

}
