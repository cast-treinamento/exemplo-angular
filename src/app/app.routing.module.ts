import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
	{
		path: 'home',
		loadChildren: './home/home.module#HomeModule',
    },
    {
		path: 'usuario',
		loadChildren: './usuario/usuario.module#UsuarioModule',
	},
	
	{ path: '', pathMatch: 'full', redirectTo: '/home' },
	// { path: '**', component: PaginaNaoEncontradaComponent, canActivate: [AuthGuard] }
];
@NgModule({
	imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
	exports: [RouterModule]
})
export class AppRoutingModule { }
